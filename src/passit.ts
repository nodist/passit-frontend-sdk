
import {Component, OnInit} from '@angular/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router';
import Controller from './controller/controller';

@Component({
    selector: 'app',
    template: require('./template/passit.html'),
    directives: [ROUTER_DIRECTIVES],
    providers: [Controller]
})

export default class Passit implements OnInit {

    constructor(private _router:Router){

    }

    ngOnInit(){

    }

}
