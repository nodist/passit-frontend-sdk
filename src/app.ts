
/**
 * Css and Scss
 */
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../lib/select2/css/select2.min.css';
import '../lib/select2/css/select2-bootstrap.min.css';
import '../lib/scss/styles.scss';

/**
 * Js
 */
import 'reflect-metadata';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';
import '../lib/select2/js/select2.min.js';

import {bootstrap} from '@angular/platform-browser-dynamic';
import {ROUTER_PROVIDERS} from '@angular/router';
import Passit from './passit';

bootstrap(Passit, [ROUTER_PROVIDERS]);
