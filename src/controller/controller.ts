
import PassitSdk from 'passit-typescript-sdk';

/**
 * Class for holding various helper classes
 */
export default class Controller {

    /**
     * The passit sdk instance
     * @type {PassitSdk}
     */
    public sdk:PassitSdk = new PassitSdk();

}
